import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Ticket } from './ticket';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  title: string = 'Concert Tickets Form';

  ticket!: Ticket;
  tickets: Ticket[] = [];

  subtitle: string = 'Entry Tickets Form';
  f_name: string = '';
  l_name: string = '';
  category: any = null;
  ticket_no: number = 0;
  total_price: number;

  constructor(private apiService: ApiService) {

    this.total_price = 0.0;

    this.apiService.getTicket().subscribe((tickets: Ticket[]) => {
      this.tickets = tickets;
    });
  }

  ngOnInit(): void {}

  onSubmit(data: any) {

    data.total_price = data.ticket_no * data.category;

    this.apiService.createTicket(data).subscribe((ticket: Ticket) => {
      this.apiService.getTicket().subscribe((tickets: Ticket[]) => {
        this.tickets = tickets;
      });
    });
  }
}
