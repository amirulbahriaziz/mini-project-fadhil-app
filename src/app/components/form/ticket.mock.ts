import { Ticket } from "./ticket";

export const TICKET: Ticket = {
  f_name: 'Amirul',
  l_name: 'Aziz',
  category: 'Accounting',
  ticket_no: 3,
  total_price: 200.0,
};
