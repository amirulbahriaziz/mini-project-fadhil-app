export interface Ticket {
	ticket_id?: number;
	f_name: string;
	l_name: string;
	category: string;
	ticket_no: number;
	total_price: number;
}
