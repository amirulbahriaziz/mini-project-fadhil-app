import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Ticket } from '../form/ticket';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  tickets: Ticket[] = [];

  constructor(private apiService: ApiService) {
    this.apiService.getTicket().subscribe((tickets: Ticket[]) => {
      this.tickets = tickets;
      console.log(this.tickets);
    })
  }

  ngOnInit(): void {
  }

}
