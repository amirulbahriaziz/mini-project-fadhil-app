import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ticket } from '../components/form/ticket';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  PHP_API_SERVER = 'http://localhost:8080/mini-project-api/api';

  constructor(private httpClient: HttpClient) {}

  createTicket(ticket: Ticket): Observable<Ticket> {
    return this.httpClient.post<Ticket>(
      `${this.PHP_API_SERVER}/create.php`,
      ticket
    );
  }

  getTicket(): Observable<Ticket[]> {
    return this.httpClient.get<Ticket[]>(`${this.PHP_API_SERVER}/read.php`);
  }
}
